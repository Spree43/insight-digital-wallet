import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;

public class AntiFraud {
  private static final String FILE_IN_BATCH = "paymo_input/batch_payment.txt";
  private static final String FILE_IN_STREAM = "paymo_input/stream_payment.txt";
  
  private static final String FILE_OUT_1 = "paymo_output/output1.txt";
  private static final String FILE_OUT_2 = "paymo_output/output2.txt";
  private static final String FILE_OUT_3 = "paymo_output/output3.txt";
  
  private static Queue<Node> queue = new LinkedList<>();
  private static HashMap<String, Node> graph = new HashMap<>();
  private static Set<Node> lstNodesToVisit = new HashSet<>();
  private static Set<Node> lstNodesToVisitNext = new HashSet<>();
  private static Map<String, Node> mapAdjacentNodes = new HashMap<>();
 
  public static void main(String[] args) throws Exception {
    createGraph();
    
    run(FILE_OUT_1, 1);
    run(FILE_OUT_2, 2);
    run(FILE_OUT_3, 4);
  }
  
  /**
   * Create the graph. This method will read a batch file and create an adjacency list data structure from the data
   * file.
   * @throws Exception
   */
  private static void createGraph() throws Exception {
    //System.out.println("Start reading historic payments...");
    
    BufferedReader br = new BufferedReader(new FileReader(FILE_IN_BATCH));
    String strLine = br.readLine();
    
    int nLineNum = 1;
    while (strLine != null && strLine.length() != 0) {
      StringTokenizer st = new StringTokenizer(strLine, ",");
      if (nLineNum == 1 || st.countTokens() < 5) {
        // not interested in this line; continue to read next line
        strLine = br.readLine();
        nLineNum++;
        continue;
      }
      
      // read the 2nd and 3rd tokens, which are the payer and payee ids
      st.nextToken(); // skip 1st token
      String strId1 = st.nextToken().trim(); // payer id
      String strId2 = st.nextToken().trim(); // payee id
      
      Node node1 = retrieveNodeFromGraph(strId1);
      Node node2 = retrieveNodeFromGraph(strId2);
      addNodesFromPathToGraph(node1, node2);

      strLine = br.readLine();
      nLineNum++;
    }
    
    br.close();
    
    //System.out.println("Done reading historic payments...");
  }
  
  /**
   * Retrieve node object from graph if found. If not, then create a new node object.
   * @param strId id
   * @return Node object
   */
  private static Node retrieveNodeFromGraph(String strId) {
    Node node = null;
    if (graph.containsKey(strId)) {
      node = graph.get(strId);
    }
    else {
      node = new Node(strId);
    }
    
    return node;
  }
  
  /**
   * Add a pair of nodes (or vertices) to the graph. The pair of nodes define an edge in the graph.
   * @param node1 node 1
   * @param node2 node 2
   */
  private static void addNodesFromPathToGraph(Node node1, Node node2) {
    node1.addAdjacentNode(node2);
    graph.put(node1.getId(), node1);
    node2.addAdjacentNode(node1);
    graph.put(node2.getId(), node2);
  }
  
  /**
   * Run the main program to determine if ids are trusted or unverified.
   * @param strOutputFile output file for the results
   * @param nDegree degree of "friendship" to be considered "trusted"
   * @throws Exception
   */
  private static void run(String strOutputFile, int nDegree) throws Exception {
    BufferedReader br = new BufferedReader(new FileReader(FILE_IN_STREAM));
    BufferedWriter bw = new BufferedWriter(new FileWriter(strOutputFile));
    
    String strLine = br.readLine();
    
    int nLineNum = 1;
    while (strLine != null && strLine.length() != 0) {
      StringTokenizer st = new StringTokenizer(strLine, ",");
      if (nLineNum == 1 || st.countTokens() < 5) {
        // not interested in this line; continue to read next line
        strLine = br.readLine();
        nLineNum++;
        continue;
      }
      
      // read the 2nd and 3rd tokens, which are the payer and payee ids
      st.nextToken();
      String strId1 = st.nextToken().trim(); // payer id
      String strId2 = st.nextToken().trim(); // payee id
      
      run(bw, nDegree, strId1, strId2, nLineNum);

      strLine = br.readLine();
      nLineNum++;
    }
    
    br.close();
    bw.close();
  }
  
  /**
   * Run the main program.
   * @param bw BufferedWriter for the output file
   * @param nDegree degree of "friendship" to be considered "trusted" 
   * @param strStart starting node
   * @param strEnd ending node
   * @param nLineNum line number from source file (for debugging)
   * @throws Exception
   */
  private static void run(BufferedWriter bw, int nDegree, String strStart, String strEnd, int nLineNum) throws Exception {
    Node nodeStart = graph.get(strStart);
    if (nodeStart == null) {
      // the starting node is not found; this means this payer has never paid before, so it will have no degrees of "friendship" with anyone
      //System.out.println("unverified" + ", line: " + nLineNum + ", degree: 0");
      bw.write("unverified");
      bw.newLine();
      return;
    }

    // add starting node to queue
    queue.clear();
    queue.add(nodeStart);
    lstNodesToVisitNext.clear();
    lstNodesToVisitNext.add(nodeStart);
    
    process(bw, nDegree, strEnd, nLineNum);
  }
  
  /**
   * Process the current transaction.
   * @param bw BufferedWriter for the output file
   * @param nDegree degree of "friendship" to be considered "trusted"
   * @param strEnd ending node
   * @param nLineNum line number from source file (for debugging)
   * @throws Exception
   */
  private static void process(BufferedWriter bw, int nDegree, String strEnd, int nLineNum) throws Exception {
    List<String> lstVisited = new ArrayList<>();
    
    
    for (int i = 1; i <= nDegree; i++) {
      // find next set of nodes to search through by following the graph
      lstNodesToVisit.clear();
      lstNodesToVisit.addAll(lstNodesToVisitNext);
      lstNodesToVisitNext.clear();
      for (Node node : lstNodesToVisit) {
        mapAdjacentNodes = node.getAdjacentNodes();
        // populate list of adjacent nodes
        for (Node nodeAdj : mapAdjacentNodes.values()) {
          if (lstVisited.contains(nodeAdj.getId()) == false) {
            lstNodesToVisitNext.add(nodeAdj);
          }
        }
      }

      // determine if ending node has been found
      if (Node.containsId(lstNodesToVisitNext, strEnd)) {
        //System.out.println("trusted" + ", line: " + nLineNum + ", degree: " + i);
        bw.write("trusted");
        bw.newLine();
        return;
      }
      // if ending node not found, and we have reached the max degree of "friendship", then end here
      else if (nDegree == i) {
        //System.out.println("unverified" + ", line: " + nLineNum + ", degree: " + i);
        bw.write("unverified");
        bw.newLine();
        return;
      }
      
      // remove nodes in current degree from queue
      int nQueueSize = queue.size();
      for (int j = 0; j < nQueueSize; j++) {
        Node nodeToRemove = queue.remove(); 
        lstVisited.add(nodeToRemove.getId()); // add node to list of visited
      }
      
      // add nodes for next degree to queue
      queue.addAll(lstNodesToVisitNext);
    }
  }
  
}

