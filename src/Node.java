import java.util.HashMap;
import java.util.Set;

public class Node {

  private String strId;
  private HashMap<String, Node> mapAdjacentNodes;
  
  Node(String strId) {
    this.strId = strId;
    mapAdjacentNodes = new HashMap<>();
  }
  
  public String getId() {
    return strId;
  }

  public void setId(String strId) {
    this.strId = strId;
  }

  public HashMap<String, Node> getAdjacentNodes() {
    return mapAdjacentNodes;
  }

  public void setAdjacentNodes(HashMap<String, Node> mapAdjacentNodes) {
    this.mapAdjacentNodes = mapAdjacentNodes;
  }
  
  public void addAdjacentNode(Node node) {
    this.mapAdjacentNodes.put(node.strId, node);
  }
  
  public static boolean containsId(Set<Node> lstNodes, String strId) {
    for (Node node : lstNodes) {
      if (strId.equals(node.getId())) {
        return true;
      }
    }
    
    return false;
  }
  
  @Override
  public String toString() {
    return this.strId;
  }
  
}
